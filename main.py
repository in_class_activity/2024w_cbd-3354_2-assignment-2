from flask import Flask, render_template, request, jsonify
import pyclamd
from google.cloud import storage
import uuid
# import mysql.connector

import os
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "gcpkey.json"

app = Flask(__name__, template_folder='template')

@app.route('/')
def index():
    return render_template('upload.html')

@app.route('/upload', methods=['POST'])
def upload_file():
    if 'file' not in request.files:
        return jsonify({'error': 'No file part'})

    file = request.files['file']

    if file.filename == '':
        return jsonify({'error': 'No selected file'})

    upload_image_to_gcs(file)

    return jsonify({'filename': file.filename})

def upload_image_to_gcs(file):
    storage_client = storage.Client()

    # Get the bucket
    bucket_name = 'file-store1'
    bucket = storage_client.bucket(bucket_name)

    # Create a blob object
    file_name = file.filename + str(uuid.uuid4())
    blob = bucket.blob(file_name)
    scan_file(file_name);
    # Upload the file directly to GCS
    blob.upload_from_file(file)

def scan_file(file_path):
    try:
        # Initialize a ClamdUnixSocket object
        clamav = pyclamd.ClamdUnixSocket()

        # Scan the file
        scan_result = clamav.scan_file(file_path)

        # Check the scan result
        if scan_result[file_path] == 'OK':
            return 'Clean'  # File is clean
        elif scan_result[file_path] == 'FOUND':
            return 'Infected'  # File is infected
        else:
            return 'Error'  # Error occurred during scanning

    except pyclamd.ConnectionError:
        return 'Error connecting to ClamAV daemon'


if __name__ == '__main__':
    app.run(debug=True)
