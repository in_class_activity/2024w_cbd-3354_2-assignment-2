# Define the provider
provider "google" {
  project = "omega-dahlia-416600"
  region  = "us-central1"
}

# Create a Google Kubernetes Engine (GKE) cluster
resource "google_container_cluster" "my_cluster" {
  name     = "my-cluster"
  location = "us-central1"

  initial_node_count = 3

  # Add node pool
  node_pool {
    name       = "default-pool"
    machine_type = "n1-standard-2"
    disk_size_gb = 100
    disk_type    = "pd-standard"
    node_count   = 3
  }
}

# Create a Google Cloud Storage bucket
resource "google_storage_bucket" "my_bucket" {
  name     = "file-store1"
  location = "us-central1"
}

# Create a Cloud SQL instance (MySQL)
resource "google_sql_database_instance" "my_instance" {
  name             = "storage"
  database_version = "MYSQL_5_7"
  region           = "us-central1"

  settings {
    tier = "db-f1-micro"
  }
}

variable "project_id" {
  description = "omega-dahlia-416600"
}

variable "region" {
  description = "The region for resources"
  default     = "us-central1"
}


project_id = "omega-dahlia-416600"


terraform init        # Initialize Terraform
terraform plan        # Review the execution plan
terraform apply       # Apply the changes to create infrastructure
